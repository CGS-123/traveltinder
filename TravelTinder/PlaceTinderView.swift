//
//  PlaceTinderViewCell.swift
//  TravelTinder
//
//  Created by Colin Smith on 3/18/17.
//  Copyright © 2017 Colin Smith. All rights reserved.
//

import UIKit
import MapKit
import SDWebImage

class PlaceTinderView: UIView {
    let regionRadius = Double(500.0)
    
    @IBOutlet weak var placeImageView: UIImageView!
    @IBOutlet weak var placeMapView: MKMapView!
    @IBOutlet weak var placeTitle: UILabel!
    @IBOutlet weak var placeLocationDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        placeImageView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func setup(with place: Place) {
        if let url = NetworkManager.imageURL(for: place) {
            placeImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "placePlaceHolderImage"))
        }
        placeTitle.text = place.title.uppercased()
        placeLocationDescription.text = place.location
        centerMapOnLocationAndAnnotate(location: CLLocation(latitude: place.latitude, longitude: place.longitude), place: place)
    }
    
    func centerMapOnLocationAndAnnotate(location: CLLocation, place: Place) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        placeMapView.setRegion(coordinateRegion, animated: true)
        let annotation = PlaceAnnotation(placeName: place.title, locationDescription: place.location, coordinate: location.coordinate)
        placeMapView.addAnnotation(annotation)
    }
}   

class PlaceAnnotation: NSObject, MKAnnotation {
    let placeName: String
    let locationDescription: String
    let coordinate: CLLocationCoordinate2D
    
    init(placeName: String, locationDescription: String, coordinate: CLLocationCoordinate2D) {
        self.placeName = placeName
        self.locationDescription = locationDescription
        self.coordinate = coordinate
        
        super.init()
    }
}
