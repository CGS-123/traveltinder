//
//  PlacesTableViewController.swift
//  TravelTinder
//
//  Created by Colin Smith on 3/18/17.
//  Copyright © 2017 Colin Smith. All rights reserved.
//

import UIKit

class PlacesTableViewController: UITableViewController {
    enum PlaceTableViewType {
        case unknown, saved, passed
    }
    
    var tableType = PlaceTableViewType.unknown
    var places = [Place]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "PlacesTableViewCell", bundle: nil), forCellReuseIdentifier: "PlacesTableViewCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if tableType != .unknown {
            updateDataSource()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateTableTypeIfNeeded()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        saveChanges()
        super.viewWillDisappear(animated)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PlacesTableViewCell", for: indexPath) as? PlacesTableViewCell else { return UITableViewCell() }
        cell.setup(with: places[indexPath.row])
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            PlacesManager.shared.unseenPlaces.append(places[indexPath.row])
            places.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }  
    }
    
    // MARK: Helpers
    
    private func updateTableTypeIfNeeded() {
        if tableType == .unknown && tabBarController?.selectedViewController == self && tabBarController?.selectedIndex == 1 {
            tableType = .saved
            updateDataSource()
        }
        if tableType == .unknown && tabBarController?.selectedViewController == self && tabBarController?.selectedIndex == 2 {
            tableType = .passed
            updateDataSource()
        }
    }
    
    private func updateDataSource() {
        switch tableType {
        case .saved:
            places = PlacesManager.shared.savedPlaces
        case .passed:
            places = PlacesManager.shared.passedPlaces
        default: break
        }
        
        tableView.reloadData()
    }
    
    private func saveChanges() {
        switch tableType {
        case .saved:
            PlacesManager.shared.savedPlaces = places 
        case .passed:
            PlacesManager.shared.passedPlaces = places
        default: break
        }
    }
}
