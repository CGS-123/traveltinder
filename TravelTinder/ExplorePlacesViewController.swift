//
//  ViewController.swift
//  TravelTinder
//
//  Created by Colin Smith on 3/18/17.
//  Copyright © 2017 Colin Smith. All rights reserved.
//

import UIKit
import Koloda

class ExplorePlacesViewController: UIViewController {
    @IBOutlet weak var tinderSwipeView: KolodaView!
    var places = [Place]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tinderSwipeView.dataSource = self
        tinderSwipeView.delegate = self
        
        if places.count == 0 {
            NetworkManager.getPlaces { [weak self] (places) in
                if let places = places {
                    self?.places = places
                    PlacesManager.shared.unseenPlaces = places
                    self?.tinderSwipeView.reloadData()
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        places = PlacesManager.shared.unseenPlaces
    }
}

extension ExplorePlacesViewController: KolodaViewDelegate {
    func kolodaDidRunOutOfCards(koloda: KolodaView) {
        tinderSwipeView.reloadData()
    }
    
    func koloda(_ koloda: KolodaView, allowedDirectionsForIndex index: Int) -> [SwipeResultDirection] {
        return [.left, .right]
    }
    
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        if direction == .right {
            PlacesManager.shared.savedPlaces.append(places[index])
            places.remove(at: index)
        } else if direction == .left {
            PlacesManager.shared.passedPlaces.append(places[index])
            places.remove(at: index)
        }
    }
}

extension ExplorePlacesViewController: KolodaViewDataSource {
    func kolodaNumberOfCards(_ koloda:KolodaView) -> Int {
        return places.count
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        guard let tinderView = Bundle.main.loadNibNamed("PlaceTinderView",
                                                                  owner: self, 
                                                                  options: nil)?[0] as? PlaceTinderView else { return UIView() }
        tinderView.setup(with: places[index])
        return tinderView
    }
}
