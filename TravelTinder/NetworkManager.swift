//
//  NetworkManager.swift
//  TravelTinder
//
//  Created by Colin Smith on 3/18/17.
//  Copyright © 2017 Colin Smith. All rights reserved.
//

import Foundation
import Alamofire

struct Place {
    enum SerializationError: Error {
        case missing(String)
    }
    
    let identifier: Int
    let title: String
    let imageURLString: String
    let location: String
    let latitude: Double
    let longitude: Double
    
    init(json: [String : Any]) throws {
        guard let identifier = json["id"] as? Int else {
            throw SerializationError.missing("identifier")
        }
        guard let title = json["title"] as? String else {
            throw SerializationError.missing("name")
        }
        guard let imageURLString = json["image"] as? String else {
            throw SerializationError.missing("imageName")
        }
        guard let location = json["location"] as? String else {
            throw SerializationError.missing("location")
        }
        guard let latitude = json["latitude"] as? Double else {
            throw SerializationError.missing("latitude")
        }
        guard let longitude = json["longitude"] as? Double else {
            throw SerializationError.missing("longitude")
        }
        
        self.identifier = identifier
        self.title = title
        self.imageURLString = imageURLString
        self.location = location
        self.latitude = latitude
        self.longitude = longitude
    }
}

class NetworkManager {
    typealias Completion = ([Place]?) -> ()
    static let shared = NetworkManager()
    static let placesURL = "https://gist.githubusercontent.com/shreyansb/678d35d7efaa4cbfb81d/raw/7e04c3d88f6c06d7a794ae570f39a96107b18457/gistfile1.json"
    
    static func getPlaces(completion: @escaping Completion) {
        Alamofire.request(placesURL).responseJSON { (response) in
            guard let places = response.result.value as? [[String : Any]] else { 
                completion(nil) 
                return 
            }
            var newPlaces = [Place]()
            for placeJSON in places {
                if let newPlace = try? Place(json: placeJSON) {
                    newPlaces.append(newPlace)
                }
            }
            
            completion(newPlaces)
        }
    }
    
    static func imageURL(for place: Place) -> URL? {
        let imageURLString = "https://travelpoker-production.s3.amazonaws.com/uploads/card/image/\(place.identifier)/\(place.imageURLString)"
        return URL(string: imageURLString)
    }
}
