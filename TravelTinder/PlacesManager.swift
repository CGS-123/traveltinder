//
//  PlacesManager.swift
//  TravelTinder
//
//  Created by Colin Smith on 3/18/17.
//  Copyright © 2017 Colin Smith. All rights reserved.
//

import Foundation

class PlacesManager {
    static let shared = PlacesManager()
    var savedPlaces = [Place]()
    var passedPlaces = [Place]()
    var unseenPlaces = [Place]()
}
